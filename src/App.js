import React, {useState, useEffect} from 'react';
import './App.css';
import Products from './components/products/Products';
import Navbar from './components/Navbar/Navbar';
import Cart from './components/Cart/Cart.jsx'
import Checkout from './components/CheckoutForm/Checkout/Checkout'
import { commerce } from './lib/commerce'
import { BrowserRouter, Route, Routes } from 'react-router-dom'; 
import Footer from './components/Footer/Footer';
import Login from './components/Auth/Login';
import Signup from './components/Auth/Signup';
import { UserAuthContextProvider } from './context/UserAuthContext';


function App() {
  const [products, setProducts] = useState([])
  const [cart, setCart] = useState({});

  const fetchProducts = async () => {
    const { data } = await commerce.products.list();

    setProducts(data)
  }


  const fetchCart = async () => {
    setCart(await commerce.cart.retrieve())
  }

  const handleAddToCart = async (productId, quantity) => {
    const { cart } = await commerce.cart.add(productId, quantity)

    setCart(cart)

  }

  const handleUpdateCartQty = async (productId, quantity) => {
    const { cart } = await commerce.cart.update(productId, {quantity})

    setCart(cart)
  }

  const handleRemoveFromCart = async (productId) => {
    const { cart } = await commerce.cart.remove(productId)
    
    setCart(cart)
  }
  
  const handleEmptyCart = async () => {
    const { cart } = await commerce.cart.empty()

    setCart(cart)
  }


  useEffect(() => {
    fetchProducts()
    fetchCart()
  }, [])


  return (
    <BrowserRouter>
        <Navbar totalItems={cart.total_items} products={products}/>
        <UserAuthContextProvider>
            <Routes>
              <Route path='/' element={<Products products={products} onAddToCart={handleAddToCart}/>}  /> 
              <Route path='/cart' element={<Cart cart={cart} 
              handleUpdateCartQty={handleUpdateCartQty}
              handleRemoveFromCart={handleRemoveFromCart}
              handleEmptyCart={handleEmptyCart}
              />} />
              <Route path='/checkout' element={<Checkout cart={cart}/>}/> 
              <Route path='/login' element={<Login/>} />
              <Route path='/Signup' element={<Signup/>} />
            </Routes>
            <Footer/>
        </UserAuthContextProvider>
    </BrowserRouter>
  );
}

export default App;
