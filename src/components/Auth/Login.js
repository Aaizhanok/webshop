import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Card, TextField, Button, Typography, Grid} from '@material-ui/core'
import './style.css'
import { useUserAuth } from '../../context/UserAuthContext'
import GoogleButton from 'react-google-button'


const Login = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')
    const {logIn, googleSignIn} = useUserAuth()
    const navigate = useNavigate()

    const handleSubmit = async (e) => {
        e.preventDefault()
        setError('')
        try {
            await logIn(email, password)
            navigate('/')
        }catch(err){
            setError(err.message)
        }
    }

    const handleGoogleSignIn = async (e) => {
        e.preventDefault()
        try { 
            await googleSignIn()
            navigate('/')
        } catch (err) {
            setError(err.message)
        }
    }


  return (
    <>
        <Grid>
            <Card className='card-login' variant='outlined' >
                <Card className='card-inputs'>
                    <Typography className='card-typography' variant='h5'>Login</Typography>
                    {error && <Typography className='error'>{error}</Typography>}
                    <form type='submit' onSubmit={handleSubmit}>
                        <div className='inputs'>
                        <TextField className='input-field' required
                            id="outlined-required"
                            label="E-mail" onChange={(e) => setEmail(e.target.value)}>
                        </TextField>
                        <TextField className='input-field'  
                            id="outlined-password-input"
                            label="Password"
                            type="password"
                            autoComplete="current-password" onChange={(e) => setPassword(e.target.value)}>
                        </TextField>
                        <Button type='submit' className='card-btn' size='small' color='primary' variant='contained'>Login</Button>
                        </div>
                        <div>
                            <GoogleButton className='g-btn' type='dark' onClick={handleGoogleSignIn}/>
                        </div>
                    </form>
                    <Card className='href-signup'>
                            <Typography className='href-typography'>Haven't an acoount?<Link to='/Signup'> SignUp here</Link></Typography>
                    </Card>
                </Card>
            </Card>
        </Grid>    
    </>
  )
}

export default Login