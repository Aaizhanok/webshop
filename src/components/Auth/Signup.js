import React, {useState} from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Card, TextField, Button, Typography, Grid, CardContent, Alert} from '@material-ui/core'
import './style.css'
import { useUserAuth } from '../../context/UserAuthContext'


const Signup = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')
    const {signUp} = useUserAuth()
    const navigate = useNavigate()

    const handleSubmit = async (e) => {
        e.preventDefault()
        setError('')
        try {
            await signUp(email, password)
            navigate('/login')
        }catch(err){
            setError(err.message)
        }
    }

  return (
    <>
        <Grid>
            <Card className='card-login' variant='outlined' >
                <Card className='card-inputs'>
                    <Typography className='card-typography' variant='h5'>Sign Up</Typography>
                    {error && <Typography className='error'>{error}</Typography>}
                    <form onSubmit={handleSubmit}>
                        <div className='inputs'>
                            <TextField className='input-field' required
                                id="outlined-required"
                                label="E-mail" 
                                onChange={(e) => setEmail(e.target.value)}>
                            </TextField>
                            <TextField className='input-field'  
                                id="outlined-password-input"
                                label="Password"
                                type="password"
                                autoComplete="current-password" 
                                onChange={(e) => setPassword(e.target.value)}>
                            </TextField>
                            <Button type='submit' className='card-btn' size='small' color='primary' variant='contained'>Sign Up</Button>
                        </div>
                    </form>
                    <Card className='href-signup'>
                        <Typography className='href-typography'>Already have an account?<Link to='/Login'> Login </Link></Typography>
                    </Card>
                </Card>
            </Card>
        </Grid>    
    </>
  )
}

export default Signup