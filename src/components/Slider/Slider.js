import React, {useState, useEffect} from 'react'
import SliderContent from './SliderContent'
import imageSlider from './ImageSlider'
import Dots from './Dots'
import './Slider.css'
import PrevNext from './PrevNext'


const len = imageSlider.length -1 

  const Slider = () => {

    const [activeIndex, setActiveIndex] = useState(0)

  useEffect(() =>{const interval = setInterval(() => {
    setActiveIndex(activeIndex === len ? 0 : activeIndex + 1)
  }, 5000)
    return () => clearInterval(interval)
  }, [activeIndex])


  return (
    <>
        <div className='slider-container'>
            <SliderContent activeIndex={activeIndex} imageSlider={imageSlider}/>
            <PrevNext 
            prevSlide={() => 
                setActiveIndex(activeIndex < 1 ? len : activeIndex -1)}
            nextSlide={() => 
                setActiveIndex(activeIndex === len ? 0 : activeIndex +1)}    
                
                />
                <Dots 
                activeIndex={activeIndex} 
                imageSlider={imageSlider} 
                onClick={activeIndex => setActiveIndex(activeIndex)}/>
        </div>
    </>
  )
}

export default Slider