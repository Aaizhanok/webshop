import React from 'react'

const SliderContent = ({ activeIndex, imageSlider}) => {
  return (
      <div className='home-container'>
      {imageSlider.map((slide, index) => (
            <div width='500px' key={index} className={index === activeIndex ? 'slides active' : 'inactive'}>
                <img className='slide-image' src={slide.urls}/>
                <h2 className='slide-title'>{slide.title}</h2>
            </div>
        ))}

      </div>  
    
  )
}

export default SliderContent