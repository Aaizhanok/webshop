export default [
    {
        title: 'SALE! SALE! SALE!',
        description: 'Поможем увеличить продажи, оптимизировать ресурсы, привести в порядок процессы, оцифровать бизнес, а также запустить стартап и новые продукты.',
        urls: 'https://i.pinimg.com/originals/f2/fc/ba/f2fcbaf0ad57af1ea48ad1eac6a627e4.jpg',
    },
    {
        title: 'Hurry up to buy',
        description: 'This is second image slider',
        urls: 'https://appgrooves.com/cdn/mc/SHOPPING/2_w1200.jpg',
    },

]