import React from 'react'

const PrevNext = ({prevSlide, nextSlide}) => {
  return (
    <div>
      <span className='prev' onClick={prevSlide}>&#10094;</span>
      <span className='next' onClick={nextSlide}>&#10095;</span>
    </div>
  )
}

export default PrevNext