import React from 'react'
import './Footer.css'
import './../../App.css'

const Footer = () => {
  return (
    <div>
      <footer className='footer'>
      <div className='container'>
        <div className='row'>
          <div className='footer-col'>
            <h4>company</h4>
            <ul className='foot-ul'>
              <li><a className='foot' href='#'>about us</a></li>
              <li><a className='foot' href='#'>our services</a></li>
              <li><a className='foot' href='#'>privacy</a></li>
              <li><a className='foot' href='#'>affiliate program</a></li>
            </ul>
          </div>

          <div className='footer-col'>
            <h4>get help</h4>
            <ul>
              <li><a href='#'>Kids-webshop</a></li>
              <li><a href='#'>Bishkek, Kyrgyzstan</a></li>
              <li><a href='#'>Vostochnaya, 150</a></li>
              <li><a href='#'>Phone: +996(702) 38–26–25</a></li>
              <li><a href='#'>Email: abd.aizhana@gmail.com</a></li>
            </ul>
          </div>
        </div>
      </div>

      </footer>
    </div>
  )
}

export default Footer