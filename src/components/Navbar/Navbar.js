import React, {useState} from 'react'
import { AppBar, Toolbar, IconButton, Badge, Typography, Button} from '@material-ui/core'
import { ShoppingCart } from '@material-ui/icons' 
import logo from './../../assets/logo.png'
import useStyles from './styles'
import './style.css'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { auth } from '../../firebase'



const  Navbar = ({ totalItems }) => {
    let [isLoggedIn, setLoggedStatus] = useState(false)
    
    const classes = useStyles()
    const location = useLocation()

    const navigate = useNavigate()
    
    const handleLogin = () => {
        navigate('/login')
    }


  return (
    <>
        <AppBar position='fixed' className={classes.appBar} color='inherit'>
            <Toolbar>
                <Typography component={Link} to='/' variant='h6' className={classes.title} color='inherit'>
                    <img src={logo} alt='Kids-webshop' height='25px' className={classes.image} />
                    Kids-webshop
                </Typography>
                <div className={classes.grow}/>
                {location.pathname === '/' && (
                    <div className={classes.button}>
                    <Button className='btn-login' type='submit' variant='contained' color='primary' onClick={handleLogin}>Login</Button>
                    <IconButton component={Link} to='/cart' aria-label='Show cart items' color='inherit'>
                        <Badge badgeContent={totalItems} color='secondary'>
                            <ShoppingCart/>
                        </Badge>
                    </IconButton>
                </div>)}
            </Toolbar>

        </AppBar>
    </>
  )
}

export default Navbar