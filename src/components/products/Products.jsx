import React, { useState } from 'react'
import { Grid,  TextField } from '@material-ui/core'
import Product from './Product/Product'
import useStyles from './style'
import Slider from '../Slider/Slider'



const Products = ( {products, onAddToCart} ) => {
    const [search, setSearch] = useState('');
    const classes = useStyles()

  return (
    <main className={classes.content}>
        <div className={classes.toolbar}/>
        <TextField type='string' variant='outlined' margin='normal' placeholder='Search...' onChange={event => {setSearch(event.target.value)}}/>
        <Grid container justify='center' spacing={4}>
            {products.filter((product) => {
                if (search === '') {
                    return product
                }else if (product.name.toLowerCase().includes(search.toLowerCase())){
                    return product
                }
            }).map((product) => (
                <Grid item key={product.id} xs={12} sm={6} md={4} lg={3}>
                    <Product product={product} onAddToCart={onAddToCart}/>
                </Grid>
            ))}
        </Grid>
        <Slider/>
    </main>
  )
}

export default Products