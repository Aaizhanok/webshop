import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth'

const firebaseConfig = {
  apiKey: "AIzaSyBoi3WWceHvkRTIWz6U3WcRnkCglyi6FQA",
  authDomain: "kids-webshop.firebaseapp.com",
  projectId: "kids-webshop",
  storageBucket: "kids-webshop.appspot.com",
  messagingSenderId: "407564088312",
  appId: "1:407564088312:web:6f8203d5d1fbb16d285c0d"
};


const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)
export default app